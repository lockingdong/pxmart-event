export default function(){
    setTimeout(()=>{
        $(".custom-scroller").mCustomScrollbar({
            theme: "rounded-dots",
            mouseWheelPixels: 80,
            scrollInertia: 0
        });
    },100);
}